//librerias
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

//nombre del proyecto
namespace carritodecompras
{
    public class CafeExpreso :bebidacomponente
    {
        public override double Costo => 12;
        public override string Descripcion => "Café expreso";
    }
}
