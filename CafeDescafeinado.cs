//libreria
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

//nombre del proyecto
namespace carritodecompras
{
    public class CafeDescafeinado:bebidacomponente
    {
        public override double Costo => 15;
        public override string Descripcion => "Café descafeinado";
    }
}
